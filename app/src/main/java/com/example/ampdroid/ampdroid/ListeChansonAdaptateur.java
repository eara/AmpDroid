package com.example.ampdroid.ampdroid;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Vector;

/**
 * Created by 1580554 on 2017-05-27.
 */


public class ListeChansonAdaptateur extends BaseAdapter {

    private Vector<Chanson> chansons;
    private Context context;

    public ListeChansonAdaptateur(Context context, Vector<Chanson> chansons) {
        this.chansons = chansons;
        this.context = context;
    }

    @Override
    public int getCount() {
        return chansons.size();
    }

    @Override
    public Object getItem(int position) {
        return chansons.elementAt(position);
    }

    @Override
    public long getItemId(int position) {
        return chansons.elementAt(position).getIdChanson();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view;
        if(convertView == null){
           view = LayoutInflater.from(context).inflate(R.layout.items_layout, parent, false);
        }
        else{
            view = convertView;
        }
        Chanson c = chansons.elementAt(position);
        TextView titre = (TextView) view.findViewById(R.id.songTitle);
        TextView artist = (TextView) view.findViewById(R.id.atiste);

        titre.setText(c.getTitre());
        artist.setText(c.getArtiste());

        return view;
    }
}
