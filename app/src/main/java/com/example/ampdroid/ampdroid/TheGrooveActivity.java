package com.example.ampdroid.ampdroid;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.support.annotation.DrawableRes;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.SeekBar;

public class TheGrooveActivity extends AppCompatActivity {

    private MusicService musicSrv;
    private boolean musicBound;
    private Intent playIntent;

    private ImageView pochette;
    private SeekBar seekBar;
    private ImageButton prevouis;
    private ImageButton playPause;
    private ImageButton next;

    private Ecouteur ec;

    //private ServiceConnection musicConnection;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_the_groove);


        ec = new Ecouteur();
        pochette = (ImageView) findViewById(R.id.pochette);
        seekBar = (SeekBar) findViewById(R.id.seekBar);
        prevouis = (ImageButton) findViewById(R.id.previous);
        playPause = (ImageButton) findViewById(R.id.playPause);
        next = (ImageButton) findViewById(R.id.next);

        pochette.setImageResource(R.drawable.iconad);
        prevouis.setImageResource(R.drawable.previous);
        playPause.setImageResource(R.drawable.pause);
        next.setImageResource(R.drawable.next);

        prevouis.setOnClickListener( ec );
        playPause.setOnClickListener( ec );
        next.setOnClickListener( ec );

        //testBtn.setText(String.valueOf(piste));

        int piste = -1;
        Log.i("devLog", "On CREATE      !!");
        Log.i("devLog","INTENT EXTRA = " + getIntent().getIntExtra("piste", piste) );

        EnsembleChanson.getInstance().setCurrent(getIntent().getIntExtra("piste", piste));
        if(musicConnection == null){
            Log.i("devLog", "CONNECTION NULL");
        }

        //Log.i("devLog", "Current = " + EnsembleChanson.getInstance(getApplicationContext()).getCurrent());
        //int piste = -1;
        //piste = getIntent().getIntExtra("piste", piste);
        //musicSrv.getDiffuseur();
    }
    //  donnée par Éric
    private ServiceConnection musicConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            MusicService.MusicBinder binder = (MusicService.MusicBinder) service;
            musicSrv = binder.getService();
            // musicSrv.setChansons(liste);
            musicBound = true;

            if(musicSrv.diffuseActuellement()){
                Log.i("devLog", "Music player is playing!!");
            }
            else{
                Log.i("devLog", "Set and play!!!");
                //playPause.setImageResource(R.drawable.pause);
                musicSrv.diffuserChanson();
            }
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            musicBound=false;
        }
    };
    /*
        Pour une raison inconu le onStart n'est pas appelé
        Ce qui a pour conséquence que le MediaPlayer.start() n'est jamais appler
    */
    @Override
    protected void onStart(){
        super.onStart();
        if ( playIntent == null)
        {
            playIntent = new Intent(this, MusicService.class);
            bindService ( playIntent, musicConnection, Context.BIND_AUTO_CREATE);
            startService(playIntent);
        }
    }

    private class Ecouteur implements View.OnClickListener{
        @Override
        public void onClick(View v) {
            if(v == playPause) {
                if (musicSrv.diffuseActuellement()) {
                    Log.i("devLog", "Music player is playing!!");
                    playPause.setImageResource(R.drawable.play);
                    musicSrv.pause();
                } else {
                    Log.i("devLog", "Set and play!!!");
                    playPause.setImageResource(R.drawable.pause);
                    //musicSrv.diffuserChanson();
                    //int piste = -1;
                    //EnsembleChanson.getInstance(getApplicationContext()).setCurrent(getIntent().getIntExtra("piste", piste));
                    //musicSrv.diffuserChanson();
                    musicSrv.demarre();
                }
            }
            else if(v == prevouis){
                Log.i("devLog", "PREVIOUS!!!");
                musicSrv.pause();
                EnsembleChanson.getInstance().playPrevious();
                musicSrv.demarre();
            }
            else if(v == next){
                Log.i("devLog", "NEXT !!!");
                musicSrv.pause();
                EnsembleChanson.getInstance().playNext();
                musicSrv.diffuseActuellement();
                musicSrv.demarre();
            }


        }
    }
}
