package com.example.ampdroid.ampdroid;

import android.provider.MediaStore;

/**
 * Created by 1580554 on 2017-05-27.
 */
public class Chanson {

    private String titre;
    private String artiste;
    private String album;
    private String albumArt;
    private int duration;
    private long id;

    /*
    MediaStore.Audio.Media._ID,  // contient les id sous forme de long
    MediaStore.Audio.Media.ALBUM,  // contient les noms d’albums sous forme de String
    MediaStore.Audio.Media.ARTIST, // contient les noms d’artistes sous forme de String
    MediaStore.Audio.Media.TITLE, // contient les titres des chansons sous forme de String
    MediaStore.Audio.Media.DURATION, // contient la durée en ms des chansons sous forme de int
    MediaStore.Audio.Albums.ALBUM, // nom d’album sous forme de String
    MediaStore.Audio.Albums.ALBUM_ART, //pochette sous forme de String
    */
    public Chanson(long id, String album, String artiste, String titre,  int duration, String albumArt ) {
        this.titre = titre;
        this.artiste = artiste;
        this.duration = duration;
        this.id = id;
        this.album = album;
        this.albumArt = albumArt;
    }

    public String getTitre() {
        return titre;
    }

    public String getArtiste() {
        return artiste;
    }

    public int getDuration() {
        return duration;
    }

    public long getIdChanson() {
        return id;
    }

    public String getAlbum() {
        return album;
    }

    public String getAlbumArt() {
        return albumArt;
    }
}
