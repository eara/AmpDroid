package com.example.ampdroid.ampdroid;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore;
import android.util.Log;

import java.util.ArrayList;
import java.util.Vector;

/**
 * Created by 1580554 on 2017-05-27.
 */
public class EnsembleChanson {

    private static EnsembleChanson instance;
    private Vector<Chanson> playlist;
    private int current;
    private int next;
    private int previous;

    private ContentResolver contentResolver;
    private String[] champsChanson = new String[]{
        MediaStore.Audio.Media._ID,  // contient les id sous forme de long
        MediaStore.Audio.Media.ALBUM,  // contient les noms d’albums sous forme de String
        MediaStore.Audio.Media.ARTIST, // contient les noms d’artistes sous forme de String
        MediaStore.Audio.Media.TITLE, // contient les titres des chansons sous forme de String
        MediaStore.Audio.Media.DURATION, // contient la durée en ms des chansons sous forme de int
        MediaStore.Audio.Media.ALBUM_ID,
        //MediaStore.Audio.Albums.ALBUM_ART, //pochette sous forme de String
    };
    private String[] champsAlbum = new String[]{
            MediaStore.Audio.Albums.ALBUM_ART, //pochette sous forme de String
    };

    /*
    private EnsembleChanson(Context context){
        //playlist = new Vector<Chanson>();
        contentResolver = context.getContentResolver();
    }
    */
    public static EnsembleChanson getInstance(){
        if(instance == null)
            instance = new EnsembleChanson();
        return instance;
    }
    /*
    public static EnsembleChanson getInstance(Context context){
        if(instance == null)
            instance = new EnsembleChanson(context);
        return instance;
    }*/

    public Vector<Chanson> getPlaylist(){
        getSdCardSong();
        return playlist;
    }

    public void addChanson(Chanson c){
        playlist.add(c);
    }

    public void setContentResolver(Context context){
        contentResolver = context.getContentResolver();
    }
    /*
    J'ai manquer de temps pour voir pourquoi le label
    de la pochette n'y ai pas.
     */
    private void getSdCardSong(){
        playlist = new Vector<Chanson>();
        Cursor curseur = contentResolver.query(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI, champsChanson, null, null, null);
        if( curseur != null ) {
            while (curseur.moveToNext()){
                long chansonId = curseur.getLong(0);
                String album = curseur.getString(1);
                String artist = curseur.getString(2);
                String titre = curseur.getString(3);
                int duration = curseur.getInt(4);
                long albumId = curseur.getLong(5);
                //String albumArt = curseur.getString(5);
                Log.i("devLog", "titre = " + titre + " id = " + chansonId);
                //Cursor c2 = contentResolver.query(MediaStore.Audio.Albums.EXTERNAL_CONTENT_URI, champsAlbum,MediaStore.Audio.Albums.ALBUM_ID, new String[]{String.valueOf(albumId)}, null);
                Chanson song = new Chanson(chansonId, album, artist, titre, duration,null);
                playlist.add(song);
            }
            curseur.close();
        }

    }

    public void setCurrent(int position){
        int nbChanson = playlist.size();
        this.current = position;
        Log.i("devLog", "Ensemble Chanson Current = " + this.current);
        if (current + 1 >= nbChanson)
            next = 0;
        else
            next = current + 1;

        if (current - 1 < 0)
            previous = nbChanson - 1;
        else
            previous = current - 1;
    }

    public long playPrevious(){
        setCurrent(current - 1);
        return getCurrent();
    }

    public long playNext(){
        setCurrent(current + 1);
        return getCurrent();
    }

    public long getCurrent(){
        long MediaId = playlist.elementAt(current).getIdChanson();
        Log.i("devLog", "Media ID = " + MediaId );
        return MediaId;
    }

}
