package com.example.ampdroid.ampdroid;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import java.util.Vector;
import java.util.jar.Manifest;

public class MediathequeActivity extends AppCompatActivity {

    private Button chansonBtn;
    private Button artisteBtn;
    private Button playlistBtn;
    private ListView liste;

    private Vector<Chanson> playlist;
    private ListeChansonAdaptateur adapter;
    private Ecouteur ec;

    private static final int PERMISSION_DEMANDE = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mediatheque);

        // Mapping des Widgets avec le layout
        chansonBtn = (Button) findViewById(R.id.songBtn);
        artisteBtn = (Button) findViewById(R.id.artistBtn);
        playlistBtn = (Button) findViewById(R.id.playlistBtn);
        liste = (ListView) findViewById(R.id.complexPlaylist);

        ec = new Ecouteur();
        liste.setOnItemClickListener( ec );

        // Demande des permission pour lire sur la carte SD
        if(checkCallingOrSelfPermission(android.Manifest.permission.READ_EXTERNAL_STORAGE ) != PackageManager.PERMISSION_GRANTED){
            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE}, PERMISSION_DEMANDE);
        }
        else{
            chargementListe();
        }
    }
    // DEMANDE DE PERMISSION
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        // Pop up de permission pour la carte SD
        if(requestCode == PERMISSION_DEMANDE) {
            // Permission autorisé par l'utilisateur
            if(grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)
            chargementListe();
        }
    }

    private void chargementListe(){
        //playlist = EnsembleChanson.getInstance(getApplicationContext()).getPlaylist();
        EnsembleChanson.getInstance().setContentResolver(getApplicationContext());
        playlist = EnsembleChanson.getInstance().getPlaylist();
        adapter = new ListeChansonAdaptateur(this, playlist);
        liste.setAdapter( adapter );
    }

    private class Ecouteur implements AdapterView.OnItemClickListener{

        @Override public void onItemClick(AdapterView<?> parent, View selected, int position, long id) {
            Log.i("devLog", String.valueOf(position));
            Intent i = new Intent(MediathequeActivity.this, TheGrooveActivity.class);
            i.putExtra("piste", position);
            startActivity(i);
        }
    }

}
